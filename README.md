# On-demand Environments-as-a-Service

Integrates as an event-driven step in your CI/CD pipeline to manage on-demand, ephemeral test environments for every
feature branch. Runs on [Uffizzi Platform](https://uffizzi.com) or your own open
source [installation](https://github.com/UffizziCloud/uffizzi_app) on Kubernetes.

## Reusable Pipeline

This repository is an includable, reusable workflow for your Gitlab Pipeline. This can handle creating, updating, and
deleting Uffizzi Environments. It will also publish environment URL's within comments on your communication platform
(e.g. Slack). We recommend using this workflow instead of the individual actions.

### Pipeline Calling Example

This example builds and publishes an image on Docker Hub. It then renders a Uffizzi Docker Compose file from a template
and caches it. It then calls the reusable pipeline to create, update, or delete the environment associated with this
Merge Request. When an environment is created, updated, or deleted, a notification is sent to the Slack channel.

You can specify a particular version of the pipeline to be connected by using tags. In the example below, version v1.0 is being used.

```yaml
include:
  - "https://gitlab.com/uffizzi/environment-action/raw/v1.0/environment.gitlab-ci.yml"
  - "https://gitlab.com/uffizzi/environment-action/raw/v1.0/Notifications/slack.yml"

variables:
  DOCKER_REPO_NAME: uffizzitest
  TAG: latest
  IMAGE_NAME: $DOCKER_REPO_NAME/app:$TAG
  UFFIZZI_COMPOSE_FILE: docker-compose.rendered.yml

.build_image: &build_image
  stage: build
  image: docker:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASSWORD
    - docker build -t $IMAGE $DOCKERFILE_PATH
    - docker push $IMAGE

services:
  - docker:dind

stages:
  - build
  - uffizzi_deploy
  - uffizzi_notification
  - uffizzi_delete

build_image:
  <<: *build_image
  variables:
    DOCKERFILE_PATH: ./vote
    IMAGE: $IMAGE_NAME

render_compose_file:
  stage: build
  image: alpine:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  before_script:
    - apk add --no-cache gettext
  script:
    - envsubst < ./docker-compose.template.yml > ./$UFFIZZI_COMPOSE_FILE
    - echo "$(md5sum $UFFIZZI_COMPOSE_FILE | awk '{ print $1 }')"
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - ./$UFFIZZI_COMPOSE_FILE
```

### Modifications of existing jobs of the pluggable pipeline

You can override existing jobs to change the behavior. For example, ignoring an error in one of the steps.

```yaml
delete_environment:
  allow_failure: true
```

In this example, we ignore an error in the delete_environment step. This can be useful when previews have a limited lifetime, but the MR is under review for a long time, and the preview associated with that MR no longer exists.

## Notifications

The _Notifications_ directory contains a pluggable pipeline for sending notifications of environment events.

If you don't want to receive notifications to your communication platform, just don't include any additional pipelines. All necessary information will be available in the _deploy_environment_ step

Example:

```yaml
include:
  - "https://gitlab.com/uffizzi/environment-action/raw/v1.0/environment.gitlab-ci.yml"
  - "https://gitlab.com/uffizzi/environment-action/raw/v1.0/Notifications/slack.yml"
```

### Pipeline Inputs

#### `UFFIZZI_USER`

**Required** Uffizzi username

#### `UFFIZZI_PASSWORD`

**Required** Uffizzi password

#### `UFFIZZI_PROJECT`

**Required** Uffizzi project name

#### `UFFIZZI_SERVER`

URL of your Uffizzi installation

#### `UFFIZZI_URL_USERNAME` and `UFFIZZI_URL_PASSWORD`

If you're controlling access to your Environments' URL's, set the credentials here so the pipeline can confirm
successful deployment.

#### `UFFIZZI_COMPOSE_FILE`

**Required** Path to a compose file within your repository

### Dockerhub Authentication

#### `DOCKERHUB_USER`

Dockerhub username

#### `DOCKERHUB_PASSWORD`

Dockerhub password or auth token

### Slack Notifications

#### `SLACK_TOKEN`

Access OAuth token for your Slack channel with `incoming-webhook` scope

<https://api.slack.com/authentication/token-types>

<https://api.slack.com/legacy/oauth-scopes>

#### `SLACK_CHANNEL`

Slack Channel ID for event notifications

#### `SLACK_WEBHOOK_URL`

Incoming webhook URL of your Slack channel for event notifications

<https://api.slack.com/messaging/webhooks>
